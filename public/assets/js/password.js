$(function(){
  $(document).on('click', '#sendEmailBtn', sendEmail);
  $(document).on('click', '#resetPasswordBtn', resetPassword);
});

var resetPassword = function(){
  var sharedData = window._sharedData,
      uiText = window._uiText,
      $alert = $('#alert')
      $password = $('#inputPassword'),
      $confirmation = $('#inputConfirmation');

  $.ajax({
    url: '/api/reset-password',
    type: 'POST',
    data: { 
      Token: sharedData.Token, 
      Password: $password.val(),
      Confirmation: $confirmation.val()
    }
  }).done(function(data, textStatus, jqXHR){
    $('.container.forget-password').html(
      '<div class="title"><br/><h4 >'
      + uiText.RESET_PASSWORD_DONE_MESSAGE  
      + '</h4></div>');

  }).fail(function(jqXHR, textStatus){
    if (jqXHR.responseJSON.Message){
      $alert.removeClass('hide').text(jqXHR.responseJSON.Message);
    }

  });
};

var sendEmail = function(){
  var $inputEmail = $('#inputEmail'),
      $alert = $('#alert'),
      sharedData = window._sharedData,
      uiText = window._uiText;

  $.ajax({
    url: '/api/reset-password',
    type: 'POST',
    data: {Email: $inputEmail.val()},
    beforeSend: function(xhr){
      $alert.addClass('hide');
    }
  }).done(function(data, textStatus, jqXHR){
    $('.container.forget-password').html('<div class="title"><br/><h4 >' + uiText.RESET_PASSWORD_SEND_EMAIL_MESSAGE  
      + '</h4><a class="btn btn-link" href="/forget-password">' + uiText.RESET_PASSWORD_SEND_EMAIL_AGAIN_MESSAGE +'</a></div>');

  }).fail(function(jqXHR, textStatus){
    if (jqXHR.responseJSON.Message){
      $alert.removeClass('hide').text(jqXHR.responseJSON.Message);
      $inputEmail.parent().addClass('has-error')
      $inputEmail.focus(function(){
        $inputEmail.parent().removeClass('has-error');
      });
    }
  });
};