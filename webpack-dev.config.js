var webpack = require('webpack');
var path = require('path');
//var liveReloadPlugin = require('webpack-livereload-plugin');

var config = {
    entry: [
            'webpack-dev-server/client?http://192.168.88.120:8080',
            'webpack/hot/only-dev-server',
            path.resolve(__dirname, 'src/client.js')
    ],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
          test: /\.js$/,
          loaders: ['react-hot', 'babel'],
          include: path.join(__dirname, 'src'),
        }, {
          test: /\.css$/, 
          loader: 'style!css'
        }, {
          test: /\.sass$/,
          loader: 'style!css!sass?indentedSyntax'
        }, {
          test: /\.(png|jpg)$/,
          loader: 'file?name=[path][name]-[hash].[ext]'
          //loader: 'url?limit=25000'
        }, {
          test: /\.(svg|ttf|woff2|woff|eot)$/,
          loader: 'file?name=[path][name]-[hash].[ext]'
          //loader: 'url?limit=100000'
        }]
    },
    plugins: [
        //new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
       })
    ]
};

module.exports = config;