var webpack = require('webpack');
var path = require('path');

var config = {
    entry: [
            //'webpack-dev-server/client?http://192.168.88.120:8080',
            //'webpack/hot/only-dev-server',
            path.resolve(__dirname, 'src/client.js')
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
          test: /\.js$/,
          loaders: ['babel'],
          include: path.join(__dirname, 'src')
        }, {
          test: /\.sass$/,
          loader: 'style!css!sass?indentedSyntax'
        }, {
          test: /\.css$/, 
          loader: 'style!css'
        }, {
          test: /\.(png|jpg)$/,
          loader: 'file?name=[path][name]-[hash].[ext]'
        }, {
          test: /\.(svg|ttf|woff2|woff|eot)$/,
          loader: 'file?name=[path][name]-[hash].[ext]'
        }]
    },
    plugins: [
        //new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
       })
    ]
};

module.exports = config;