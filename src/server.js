var path = require('path'),
    request = require('request'),
    express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    moment = require('moment'),
    i18n = require('i18n');

var redirect = require('./routes/redirect'),
    api = require('./routes/api'),
    capso = require('./routes/capso'),
    user = require('./routes/user'),
    password = require('./routes/password'),
    Meta = require('./util/meta'),
    Config = require('./config/config.json'),
    appDownload = require('./config/appDownload.json'),
    lang = require('./locales/lang.json');

i18n.configure({
  locales:['en', 'zh-TW'],
  cookie: 'lang',
  directory: __dirname + '/locales',
  updateFiles: false,
  defaultLocale: 'zh-TW',
  extension: '.js'
});

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, '../public')));
app.use(i18n.init);

app.use(session({
  secret: 'capso is great!',
  cookie: { maxAge: 60000 }, 
  resave: true, 
  saveUninitialized: true
}));

app.use(function (req, res, next) {
  res.locals.path = req.path;

  if(req.query.l && req.query.l in lang){
    req.session.locale = req.query.l;
  }

  if (req.session.locale) {
    req.setLocale(req.session.locale);
  }

  res.locals.currentLang = lang[i18n.getLocale(req)];
  next();
});

app.get('/', function (req, res) {
  
  var appLinks = {}, meta = {};

  switch(i18n.getLocale(req)){
    case "zh-TW":
      appLinks.HomeTop = appDownload.iOS.tw.zh.HomeTop;
      appLinks.HomeBottom = appDownload.iOS.tw.zh.HomeBottom;
      break;

    default:
      appLinks.HomeTop = appDownload.iOS.tw.en.HomeTop;
      appLinks.HomeBottom = appDownload.iOS.tw.en.HomeBottom;
  }

  meta.ogUrl = Config.hostname + req.url;

  res.render('home/index', { appLinks: appLinks, meta: Meta.basic(meta) });
});

app.get('/terms-of-use', function (req, res) {

  var meta = {};
  meta.ogUrl = Config.hostname + req.url;
  res.render('home/terms-of-use', { meta: Meta.basic(meta) });
});

app.get('/privacy-policy', function (req, res) {
  var meta = {};
  meta.ogUrl = Config.hostname + req.url;
  res.render('home/privacy-policy', { meta: Meta.basic(meta) });
});

app.get('/attribution-statement', function (req, res) {
  var meta = {};
  meta.ogUrl = Config.hostname + req.url;
  res.render('home/attribution-statement', { meta: Meta.basic(meta) });
});

app.get('/r/:dest', redirect.go );

app.get('/capso/:id', capso.get );

//app.get('/users/:id', user.get );  

app.get('/forget-password', password.forget);

app.get('/reset-password/:token', password.reset);

app.post('/api/reset-password', api.resetPassword);

var server = app.listen(process.env.PORT || 3000, function () {
  console.log(server.address());
  var host = server.address().address;
  var port = server.address().port;

  console.log('App listening at http://%s:%s', host, port);
});
