{ 
  "META_TITLE": "Capso - Unlock the Secret of the Location",
  "META_DESCRIPTION": "Capso allows you to capture the wonderful moment as the treasure, and attracts friends to the location to explore. You can also follow the video and treasure map, start your journey to unlock the secrets of the locations, and meet new friends who preserved the secrets.",
  "ABOUT_CAPSO": "About Capso",
  "CAPSO_SUBTITLE": "Unlock the Secret of the Location.",
  "CAPSO_DESCRIPTION": "Capso allows you to capture the wonderful moment as the treasure, and attracts friends to the location to explore. You can also follow the video and treasure map, start your journey to unlock the secrets of the locations, and meet new friends who preserved the secrets.",
  "CAPSO_ROLES_TITLE": "Capso has two roles, Treasure Creator and Treasure Explorer.",
  "TREASURE_CREATOR": "Treasure Creator",
  "TREASURE_CREATOR_SUBTITLE": "One who preserves and share <br/> the beautiful moments.",
  "TREASURE_CREATOR_DESCRIPTION": "Creator’s mission is capturing the wonderful moment through video and preserving the secret at the location. Others can view your video story, and unlock the secret you preserved at the location.",
  "TREASURE_CREATOR_FEATURE_01": "Capture",
  "TREASURE_CREATOR_FEATURE_01_DESCRIPTION": "Capture the wonderful moment and record a 6 to 30 seconds video.",
  "TREASURE_CREATOR_FEATURE_02": "Preserve",
  "TREASURE_CREATOR_FEATURE_02_DESCRIPTION": "Preserve the video secret at the location.",
  "TREASURE_CREATOR_FEATURE_03": "Share",
  "TREASURE_CREATOR_FEATURE_03_DESCRIPTION": "Share video and map as the treasure clues for others.",
  "TREASURE_EXPLORER": "Treasure Explorer",
  "TREASURE_EXPLORER_SUBTITLE": "One who explores the world, experiences <br/> the touching and beautiful moments.",
  "TREASURE_EXPLORER_DESCRIPTION": "Explorer can explore the treasure around the world after receiving video treasure map from friends. The more friends you have, the more treasure clues you receive. You can also interact with your friends through likes or comments in Capso.",
  "TREASURE_EXPLORER_FEATURE_01": "Receive",
  "TREASURE_EXPLORER_FEATURE_01_DESCRIPTION": "Receive the video and treasure clues from your friends in real-time.",
  "TREASURE_EXPLORER_FEATURE_02": "Interact",
  "TREASURE_EXPLORER_FEATURE_02_DESCRIPTION": "Interact with creator and explorers through likes or comments.",
  "TREASURE_EXPLORER_FEATURE_03": "Explore",
  "TREASURE_EXPLORER_FEATURE_03_DESCRIPTION": "Follow the treasure map to unlock the secrets of the location, and be surprised!",
  "CONTACT_&_PRESS_KIT": "Contact & Press Kit",
  "EMAIL": "Email",
  "FOLLOW_US_ON_FACEBOOK": "Follow us on Facebook",
  "DOWNLOAD_PRESS_KIT": "Download press kit",
  "EMAIL_SUBSCRIPTION_DESCRIPTION": "Capso is currently available on Taiwan App Store only. Leave your Email to get the latest news when Capso is available in your country.",
  "SUBMIT": "SUBMIT",
  "THANK_YOUR_SUBSCRIPTION!": "Thank you!",
  "APP_FOOTER_DESCRIPTION": "Start to preserve and explore treasure!",
  "CONTACT_US": "Contact Us",
  "TERMS_OF_USE": "Terms of Use",
  "PRIVACY_POLICY": "Privacy Policy",
  "ATTRIBUTION_STATEMENT": "Attribution Statement",
  "CAPSO_META_TITLE": "{{userName}} has preserved a secret at {{locationName}}. Unlock it with Capso!",
  "CAPSO_SECRET_POPUP_TITLE": "has preserved a Capso at",
  "CAPSO_SECRET_POPUP_SUBTITLE": "Unlock the secret with Capso!",
  "DOWNLOAD_APP": "Get App",
  "DOWNLOAD_NOW": "Download",
  "OPEN_TREASURE": "Unlock",
  "ERROR_TITLE": "Ooops! This page is not available now.",
  "ERROR_MESSAGE": "The link may be invalid or the page may be removed.",
  "ERROR_BACK_TO_WEBSITE": "Go back to Capso website.",
  "Forgot your password?": "Forgot your password?",
  "Enter your email below to reset password.": "Enter your email below to reset password.",
  "Please select alternative login method.": "Please select alternative login method.",
  "Email address is invalid.": "Email address is invalid.",
  "RESET_PASSWORD_SEND_EMAIL_MESSAGE": "We've sent you an email with a link to reset your password. Please check your email.",
  "RESET_PASSWORD_SEND_EMAIL_AGAIN_MESSAGE" : "Receive yet? Send again!",
  "Reset password": "Reset password",
  "Enter your new password.": "Enter your new password.",
  "New password": "New password",
  "Re-enter password": "Re-enter password",
  "RESET_PASSWORD": "Reset",
  "Password length must be longer than 8 characters.": "Password length must be longer than 8 characters.",
  "Password and its confirmation do not match.": "Password and its confirmation do not match.",
  "Invalid hex string": "Invalid hex string",
  "Password reset token has expired.": "Password reset token has expired.",
  "RESET_PASSWORD_DONE_MESSAGE": "Successfully reset password! Please login again."
}