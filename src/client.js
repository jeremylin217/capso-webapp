/* client.js */

import React from 'react'
import ReactDOM from 'react-dom'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import { Router, Route, IndexRoute } from 'react-router'
import SUI from '../semantic/dist/semantic.min'
import Capso from './components/Capso'
import '../semantic/dist/semantic.min.css'
import 'purecss/build/pure-min.css'

import './sass/app.sass'
import './sass/user.sass'

ReactDOM.render(
  <Router history={createBrowserHistory()}>
    <Route path='/capso/:id' component={Capso}/>
  </Router>
  , document.getElementById('reactRoot')
);