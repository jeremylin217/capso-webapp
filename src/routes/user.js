var request = require('request'),
    moment = require('moment'),
    i18n = require('i18n');

var Meta = require('../util/meta'),
    UiTextHandler = require('../util/uiText'),
    config = require('../config/config.json'),
    accessToken = require('../util/accessToken'),
    appDownload = require('../config/appDownload.json');

exports.get = function(req, res) {
  var userId = req.params.id;
  var options = {
    url: config.apiUrl + '/v1.0/users/' + userId,
    headers: {
      'Authorization': 'Bearer ' + accessToken.get()
    }
  };

  var callback = function(error, response, body){
    var sharedData = {}, 
        uiText = {}, 
        meta = {}, 
        _Error, userData;
    
    if (error || response.statusCode !== 200) {
      _Error = {
        ERROR_TITLE : req.__('ERROR_TITLE'),
        ERROR_MESSAGE : req.__('ERROR_MESSAGE'),
        ERROR_BACK_TO_WEBSITE : req.__('ERROR_BACK_TO_WEBSITE')
      };

      var outputData = { 
        sharedData: sharedData,
        _Error: _Error, 
        meta : Meta.basic(meta), 
        uiText: uiText 
      };

      res.render('user', outputData);
      return;
    }

    userData = JSON.parse(body);
    sharedData.userData = userData;
    var url = config.hostname + '/users/' + userData.Id;

    meta = Meta.basic(meta);
    meta.canonical = url;
    meta.ogUrl = url;
    meta.alWebUrl = url;
    meta.aliOSUrl = 'capso://users/' + userData.Id;

    var outputData = { 
      sharedData: sharedData, 
      meta: meta, 
      uiText: uiText
    };

    res.render('user', outputData);
  };

  request(options, callback);
};