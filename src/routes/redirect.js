var urls = {
  "home": "https://capso.cc",
  "appstore" : "https://itunes.apple.com/tw/app/capso/id985646492",
  "faq": "https://goo.gl/Vqqm9z",
  "facebook": "https://www.facebook.com/capsoHQ"
};

var except = [
  "utm_source", 
  "utm_medium", 
  "utm_content", 
  "utm_campaign",
  "utm_term"
];

exports.go = function(req, res, next) {
  var dest = req.params.dest;

  if( !(dest in urls) ) {
    return next();
  }

  var url = urls[dest], 
      isFirst = true;

  if( dest === 'appstore' ) {
    url += '?pt=117757144';
    isFirst = false;
  }

  for (var prop in req.query) {
    if( except.indexOf(prop) === -1 ) {
      url += (isFirst) ? '?' : '&';
      url += prop + '=' + req.query[prop];
      isFirst = false;
    }
  }

  res.render('redirect', { url : url });
  return next();
  
};
