var i18n = require('i18n');

var config = require('../config/config.json'),
    UiTextHandler = require('../util/uiText');

exports.forget = function(req, res) {
  var uiText = {};
  uiText = UiTextHandler.resetPassword(uiText);
  res.render('password/forget', { uiText: uiText });
};

exports.reset = function(req, res) {
  var sharedData = {
    Token: req.params.token
  }, uiText = {};

  uiText = UiTextHandler.resetPassword(uiText);
  res.render('password/reset', { sharedData: sharedData, uiText: uiText });
};