var request = require('request'),
    i18n = require('i18n'),
    config = require('../config/config.json');

exports.resetPassword = function(req, res){
  var options = {
    url: config.apiUrl + '/v1.0/reset-password',
    form: req.body
  };

  var callback = function(error, response, body){
    if(response.statusCode !== 204){
      var data = JSON.parse(body);
      delete data.Error.Stack;
      data.Error.Message = i18n.__(data.Error.Message);
      res.status(response.statusCode).json(data.Error);
      return;
    }

    res.status(response.statusCode).json({});
    return;
  };

  request.post(options, callback);
};


