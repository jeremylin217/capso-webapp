var request = require('request'),
    moment = require('moment'),
    i18n = require('i18n');

var Meta = require('../util/meta'),
    UiTextHandler = require('../util/uiText'),
    config = require('../config/config.json'),
    accessToken = require('../util/accessToken'),
    appDownload = require('../config/appDownload.json');

exports.get = function(req, res) {
  var capsoId = req.params.id;
  var options = {
    url: 'https://api.capso.cc/v1.0/capsules/' + capsoId,
    headers: {
      'Authorization': 'Bearer ' + accessToken.get()
    }
  };

  var callback = function(error, response, body){
    var sharedData = {}, 
        uiText = {}, 
        meta = {}, 
        _Error, capsoData;

    uiText = UiTextHandler.capsoPage(uiText);
    uiText = UiTextHandler.footer(uiText);

    if (error || response.statusCode !== 200) {

      _Error = {
        ERROR_TITLE : req.__('ERROR_TITLE'),
        ERROR_MESSAGE : req.__('ERROR_MESSAGE'),
        ERROR_BACK_TO_WEBSITE : req.__('ERROR_BACK_TO_WEBSITE')
      };

      var appLinks = {};
      switch(i18n.getLocale(req)){
        case "zh-TW":
          appLinks.CapsoPageTop = appDownload.iOS.tw.zh.CapsoPageTop;
          break;

        default:
          appLinks.CapsoPageTop = appDownload.iOS.tw.en.CapsoPageTop;
      }
      sharedData.AppLinks = appLinks;

      var outputData = { 
        sharedData: sharedData,
        _Error: _Error, 
        meta : Meta.basic(meta), 
        uiText: uiText 
      };

      res.render('capso', outputData);
      return;
    }

    capsoData = JSON.parse(body);

    if ( "Error" in capsoData || capsoData.Permission !== 'public') {

      _Error = {
        ERROR_TITLE : req.__('ERROR_TITLE'),
        ERROR_MESSAGE : req.__('ERROR_MESSAGE'),
        ERROR_BACK_TO_WEBSITE : req.__('ERROR_BACK_TO_WEBSITE')
      };

      var appLinks = {};
      switch(i18n.getLocale(req)){
        case "zh-TW":
          appLinks.CapsoPageTop = appDownload.iOS.tw.zh.CapsoPageTop;
          break;

        default:
          appLinks.CapsoPageTop = appDownload.iOS.tw.en.CapsoPageTop;
      }
      sharedData.AppLinks = appLinks;

      var outputData = { 
        sharedData: sharedData,
        _Error: _Error, 
        meta : Meta.basic(meta), 
        uiText: uiText 
      };

      res.render('capso', outputData);
      return;
    }

    var appLinks = {};
    switch(i18n.getLocale(req)){
      case "zh-TW":
        appLinks.CapsoPageTop = appDownload.iOS.tw.zh.CapsoPageTop;
        appLinks.CapsoPageSecret = appDownload.iOS.tw.zh.CapsoPageSecret;
        break;

      default:
        appLinks.CapsoPageTop = appDownload.iOS.tw.en.CapsoPageTop;
        appLinks.CapsoPageSecret = appDownload.iOS.tw.en.CapsoPageSecret;
    }
    sharedData.AppLinks = appLinks;

    var url = config.hostname + '/capso/' + capsoData.Id;
    capsoData.Url = url;
    capsoData.Updated = moment(capsoData.Updated).calendar();
    sharedData.CapsoData = capsoData;

    var metaTitle = res.__('CAPSO_META_TITLE', { userName: capsoData.User.Name, locationName: capsoData.Location.Name});
    var metaType = 'video';
    meta = {
      canonical: url,
      title: metaTitle,
      medium: metaType,
      ogSiteName: 'Capso',
      ogTitle: metaTitle,
      ogImage: capsoData.Preview,
      ogImageType: 'image/jpeg',
      ogImageWidth: 720,
      ogImageHeight: 720,
      ogDescription: capsoData.Description,
      ogUrl: url,
      ogType: 'video.movie',
      ogVideo: capsoData.Media[0].Url.replace("https://", "http://"),
      //ogVideoUrl: capsoData.Media[0].Url.replace("https://", "http://"),
      ogVideoSecureUrl: capsoData.Media[0].Url,
      ogVideoType: 'video/mp4',
      ogVideoWidth: 720,
      ogVideoHeight: 720,
      aliOSUrl: 'capso://capso/' + capsoData.Id,
      alWebUrl: url
    };

    var outputData = { 
      sharedData: sharedData, 
      meta: meta, 
      uiText: uiText
    };

    res.render('capso', outputData);
  }

  request(options, callback);

};