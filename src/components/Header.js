import React from 'react';

var Header = React.createClass({
  render: function() {
    let {DOWNLOAD_APP} = window._uiText,
        {AppLinks} = window._sharedData;
    return (
        <header id="header" className="ui large secondary menu menufixed">
          <div className="ui container">
            <a href="/" className="header item">
              <img className="logo" src="/images/capso-logo.svg" />
            </a>
            <div className="right floated item">
              <a href={AppLinks.CapsoPageTop} target="_blank" className="ui tiny inverted basic button">{DOWNLOAD_APP}</a>
            </div>
          </div>
        </header>
    );
  }
});

export default Header;