import React from 'react';
import {GoogleMap, Marker} from "react-google-maps";
import Header from './Header';
import Footer from './Footer';

var Error = React.createClass({
  render: function() {
    let {ERROR_TITLE, ERROR_MESSAGE, ERROR_BACK_TO_WEBSITE} = window._Error;
    return (
        <div id="error" className="ui container">
          <article >
            <h1>{ERROR_TITLE}</h1>
            <p>{ERROR_MESSAGE}</p>
            <p><a href="//capso.cc">{ERROR_BACK_TO_WEBSITE}</a></p>
          </article>
        </div>
      )
  }
});

var Main = React.createClass({
  render: function() {
    return (
        <div id="main" className="ui container">
          <CapsoContainer />
          <SecretContainer />
        </div>
      )
  }
});

var SecretContainer = React.createClass({
  handleClick: function() {
    $('#secretContainer').modal('hide');
  },
  render: function() {
    let {User, Location} = window._sharedData.CapsoData,
        {AppLinks} = window._sharedData,
        {CAPSO_SECRET_POPUP_TITLE, CAPSO_SECRET_POPUP_SUBTITLE, DOWNLOAD_APP} = window._uiText;
    return (
        <div id="secretContainer" className="ui basic modal">
          <article>
            <img id="app-icon" src="/images/app-icon.png" />
            <p className="description">{User.Name} {CAPSO_SECRET_POPUP_TITLE} <br/><span className="location-name">{Location.Name}</span><br/>
            {CAPSO_SECRET_POPUP_SUBTITLE}</p>
            <a href={AppLinks.CapsoPageSecret} target="_blank" className="ui basic golden button">
              {DOWNLOAD_APP}
            </a>
          </article>
        </div>
      );
  }
});

var MediaContainer = React.createClass({
  componentDidMount() {
    let video = document.getElementById('video');
    video.onended = function() {
      $('#secretContainer').modal('show');
    }
  },

  playVideo: function() {
    let video = document.getElementById('video');
    video.play(); 
  },

  pauseVideo: function() {
    let video = document.getElementById('video');
    if(video.paused){
      video.play();
    }else{
      video.pause();
    }
  },

  render: function() {
    let media = this.props.media,
        preview = this.props.preview;
    return (
        <div id="media" className="ui square">
          <img className="default" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAAtJREFUCB1jYAACAAAFAAGNu5vzAAAAAElFTkSuQmCC" />
          <img className="preview" src={preview} onClick={this.playVideo} />
          <video controls id="video" className="video" onClick={this.pauseVideo} width="100%" height="100%">
            <source src={media[0].Url} type="video/mp4" ref="video" />
          </video>
        </div>
    );
  }
});

var CapsoContainer = React.createClass({
  getInitialState: function(){
    return { data: {} };
  },
  nl2br: function (text) {
      var regex = /(\n)/g;
      return text.split(regex).map(function (line) {
          if (line.match(regex)) {
              return React.createElement('br')
          }
          else {
              return line;
          }
      });
  },
  render: function() {
    let CapsoData = window._sharedData.CapsoData;
    return (
        <div id="capsoContainer">
        <article>
          <div id="userInfo" >
            <div className="ui horizontal list">
              <div className="item">
                <img src={CapsoData.User.Picture} className="ui avatar mini circular image" />
                <div className="content">
                  <div className="ui sub user-name">{CapsoData.User.Name}</div>
                  <div className="ui sub datetime">{CapsoData.Updated}</div>
                </div>
              </div>
            </div>
          </div>
          <MediaContainer media={CapsoData.Media} preview={CapsoData.Preview} />
          <div id="mediaInfo">
            <div className="ui middle aligned list">
              <div id="location" className="item">
                <img className="ui image icon-marker" src="/images/icon_marker.svg" />
                <div id="location-name" className="content">{CapsoData.Location.Name}</div>
                <div className="right floated content">
                  <SercetOpenButton />
                </div>
              </div>
              <div id="mapContainer" className="item">
                <GoogleMap 
                  containerProps={{
                    style: {
                      borderRadius: "4px",
                      height: "100%"
                    }
                  }}
                  defaultZoom={13}
                  defaultCenter={{lat: parseFloat(CapsoData.Location.Latitude), lng: parseFloat(CapsoData.Location.Longitude)}} 
                  defaultOptions={{draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true}}>
                  <Marker
                    defaultPosition={{lat: parseFloat(CapsoData.Location.Latitude), lng: parseFloat(CapsoData.Location.Longitude)}}
                    title={CapsoData.Location.Name}/>
                </GoogleMap>
              </div>
              <div id="description" className="item">{this.nl2br(CapsoData.Description)}</div> 
            </div>
            <div id="comments">
            </div>
            <div id="addComment">
            </div>
          </div>
        </article>
        </div>
    );
  }
});

var SercetOpenButton = React.createClass({
  handleClick: function(event) {
    $('#secretContainer').modal('show');
  },

  render: function() {
    let {OPEN_TREASURE} = window._uiText;
    return (
        <button className="ui basic golden button" onClick={this.handleClick}>{OPEN_TREASURE}</button>
      ); 
  }
});

var CapsoPage = React.createClass({

  getInitialState() {
    let isError = (typeof window._Error !== 'undefined') ? true : false;
    return {
        isError: isError
    };
  },

  componentDidMount() {
    this._loadJs();
  },

  componentDidUpdate() {

  },

  _loadJs() {
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=941855375854709";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

    (function(d, s, id){
      var js = d.createElement(s);
      js.src = "//assets.pinterest.com/js/pinit.js";
      d.body.appendChild(js);
    }(document, 'script', 'pinterest-jssdk'));
  },

  render: function() {
      return (
          <div id="page">
            <Header />
             {this.state.isError ? <Error /> : <Main />}
             {this.state.isError ? '' : <div id="socialBtns" className="ui container">
              <div className="fb-like" data-href={window._sharedData.CapsoData.Url} data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
              <div className="twitter-btn"><a href="https://twitter.com/share" className="twitter-share-button" data-hashtags="Capso"></a></div>
              <div className="pinterest-btn"><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-color="red"></a></div>
             </div>}
            <Footer />
          </div>
      );
  }
});

export default CapsoPage;