import React from 'react';

var Footer = React.createClass({
  render: function() {
    let {CONTACT_US, TERMS_OF_USE, PRIVACY_POLICY, ATTRIBUTION_STATEMENT} = window._uiText;
    return (
      <footer id="footer" className="ui container">
        <p className="links">
          <a href="mailto:support@capso.cc" className="terms-link">{CONTACT_US}</a>
          <span className="split">|</span>
          <a href="//capso.cc/terms-of-use" className="terms-link">{TERMS_OF_USE}</a>
          <span className="split">|</span>
          <a href="//capso.cc/privacy-policy" className="terms-link">{PRIVACY_POLICY}</a>
          <span className="split">|</span>
          <a href="//capso.cc/attribution-statement" className="terms-link">{ATTRIBUTION_STATEMENT}</a>
        </p>
        <p className="copyright">© 2015 Capso Co.,Ltd.</p>
      </footer>
    );
  }
});

export default Footer;