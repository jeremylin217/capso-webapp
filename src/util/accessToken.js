var jwt = require('jsonwebtoken');

var secret = "M7aKdEO8mZT9ORjFPPpXcn6bmm4=";
// sign with default (HMAC SHA256)
var accessToken = jwt.sign({ 
                        "iat": Date.now(),
                        "exp": Date.now()+300,
                        "iss": "4966369200177152"
                    }, secret);

exports.get = function() {

  return accessToken;
};