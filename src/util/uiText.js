var i18n = require('i18n');

exports.footer = function(uiText) {
  uiText.CONTACT_US = i18n.__('CONTACT_US');
  uiText.TERMS_OF_USE = i18n.__('TERMS_OF_USE');
  uiText.PRIVACY_POLICY = i18n.__('PRIVACY_POLICY');
  uiText.ATTRIBUTION_STATEMENT = i18n.__('ATTRIBUTION_STATEMENT');
  return uiText

};

exports.capsoPage = function(uiText) {
  uiText.DOWNLOAD_APP = i18n.__('DOWNLOAD_APP');
  uiText.DOWNLOAD_NOW = i18n.__('DOWNLOAD_NOW');
  uiText.CAPSO_SECRET_POPUP_TITLE = i18n.__('CAPSO_SECRET_POPUP_TITLE');
  uiText.CAPSO_SECRET_POPUP_SUBTITLE = i18n.__('CAPSO_SECRET_POPUP_SUBTITLE');
  uiText.OPEN_TREASURE = i18n.__('OPEN_TREASURE');
  return uiText;

};

exports.resetPassword = function(uiText) {
  uiText.RESET_PASSWORD_SEND_EMAIL_MESSAGE = i18n.__('RESET_PASSWORD_SEND_EMAIL_MESSAGE');
  uiText.RESET_PASSWORD_SEND_EMAIL_AGAIN_MESSAGE = i18n.__('RESET_PASSWORD_SEND_EMAIL_AGAIN_MESSAGE');
  uiText.RESET_PASSWORD_DONE_MESSAGE = i18n.__('RESET_PASSWORD_DONE_MESSAGE');
  return uiText;

};
