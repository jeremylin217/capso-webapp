var i18n = require('i18n'),
    CONFIG = require('../config/config.json');

exports.basic = function(meta) {

  var metaData = {
    title: i18n.__('META_TITLE'),
    description: i18n.__('META_DESCRIPTION'),
    ogSiteName: "Capso",
    ogTitle: i18n.__('META_TITLE'),
    ogDescription: i18n.__('META_DESCRIPTION'),
    ogImage: CONFIG.hostname + '/images/facebook-share-image.jpg',
    ogType: 'website'
  };

  if(meta.ogUrl)
    metaData.ogUrl = meta.ogUrl;
  
  return metaData;
};