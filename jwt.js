var request = require('request');
var jwt = require('jsonwebtoken');

var secret = "M7aKdEO8mZT9ORjFPPpXcn6bmm4=";
// sign with default (HMAC SHA256)
var accessToken = jwt.sign({ 
                        "iat": Date.now(),
                        "exp": Date.now()+300,
                        "iss": "4966369200177152"
                    }, secret);

var capsoId = '6353436189384386';
var options = {
  url: 'https://api.capso.cc/v1.0/users/4966369200177152',
  headers: {
    'Authorization': 'Bearer ' + accessToken
  }
};

var callback = function(error, response, body){
  if (!error && response.statusCode == 200) {
    var jsonData = JSON.parse(body);
    console.log(jsonData);

  }else{

    console.log({});
  }
}

request(options, callback);