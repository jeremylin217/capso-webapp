var webpack = require('webpack');
var path = require('path');

var config = {
    entry: [
        path.resolve(__dirname, 'src/client-user.js')
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'client-user.js'
    },
    module: {
        loaders: [{
          test: /\.js$/,
          loaders: ['babel'],
          include: path.join(__dirname, 'src')
        }, {
          test: /\.sass$/,
          loader: 'style!css!sass?indentedSyntax'
        }, {
          test: /\.css$/, 
          loader: 'style!css'
        }, {
          test: /\.(png|jpg)$/,
          loader: 'file?name=[path][name]-[hash].[ext]'
        }, {
          test: /\.(svg|ttf|woff2|woff|eot)$/,
          loader: 'file?name=[path][name]-[hash].[ext]'
        }]
    },
    plugins: [
        //new webpack.HotModuleReplacementPlugin(),
       //  new webpack.NoErrorsPlugin(),
       //  new webpack.ProvidePlugin({
       //     $: "jquery",
       //     jQuery: "jquery"
       // })
    ]
};

module.exports = config;